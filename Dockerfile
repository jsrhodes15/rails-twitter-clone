FROM ruby:2.5.1-alpine

LABEL maintainer="Jordan Rhodes"

# Get the stuff we need to run Rails
RUN apk update -qq  && apk add build-base nodejs postgresql-dev

WORKDIR /app

# Install dependencies
COPY Gemfile Gemfile.lock ./
RUN bundle install

# Copy app code
COPY . /app

# Configure entry point so we don't need to specify 'bundle exec' for each of our commands
# can  override with --entrypoint="" from command line when needed
ENTRYPOINT [ "bundle", "exec" ]

# Default command if none specified when starting up
CMD ["rails", "server", "-p", "3000", "-b", "0.0.0.0"]
