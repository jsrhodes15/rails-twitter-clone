# Dockerized Ruby on Rails Tutorial sample application

This is the sample application for
[*Ruby on Rails Tutorial:
Learn Web Development with Rails*](http://www.railstutorial.org/)
by [Michael Hartl](http://www.michaelhartl.com/).

## License

All source code in the [Ruby on Rails Tutorial](http://railstutorial.org/)
is available jointly under the MIT License and the Beerware License. See
[LICENSE.md](LICENSE.md) for details.

## Getting started

### Getting started with Docker
_assumes you have docker installed on your machine_

Clone the repo and build images
```
$ cd rails-twitter-clone && docker-compose up
```

Build and migrate database
_in a separate terminal_
```
$ docker-compose run app rake db:create
```
```
$ docker-compose run app rake db:migrate
```

Run tests
```
$ docker-compose run app rails t
```

### "The other way"
To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install --without production
```

Next, migrate the database:

```
$ rails db:migrate
```

Finally, run the test suite to verify that everything is working correctly:

```
$ rails test
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ rails server
```

For more information, see the
[*Ruby on Rails Tutorial* book](http://www.railstutorial.org/book).
